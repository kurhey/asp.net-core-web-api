﻿using System.Net.Mail;
using System.Threading.Tasks;
using System.Net;

namespace VetrovShop.WEB
{
    // This class is used by the application to send email for account confirmation and password reset.
    // For more details see https://go.microsoft.com/fwlink/?LinkID=532713
    public class EmailSender : IEmailSender
    {
        public async Task SendEmailAsync(string email, string subject, string message)
        {
            var mail = new MailMessage()
            {
                From = new MailAddress("jakson.anno@gmail.com", "Evgeni Vetrov")
            };

            mail.To.Add(new MailAddress(email));

            mail.Subject = "Personal Management System - " + subject;
            mail.Body = message;
            mail.IsBodyHtml = true;
            mail.Priority = MailPriority.High;

            using (SmtpClient smtp = new SmtpClient("smtp.gmail.com", 587))
            {
                smtp.Credentials = new NetworkCredential("jakson.anno@gmail.com", "Vetrov3005");
                smtp.EnableSsl = true;
                await smtp.SendMailAsync(mail);
            }
        }
    }
}