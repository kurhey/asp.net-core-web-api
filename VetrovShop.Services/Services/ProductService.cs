﻿using System.Collections.Generic;
using VetrovShop.Models;
using VetrovShop.Services.Interfaces;
using VetrovShop.DAL.Interfaces;
using System;
using System.Linq;

namespace VetrovShop.Services
{
    public class ProductService : IProductService
    {
        IUnitOfWork Database { get; set; }

        public ProductService(IUnitOfWork unitOfWork)
        {
            Database = unitOfWork;
        }

        public Product FindProduct(int id)
        {
            try
            {
                return Database.Products.FindById(id);
            }
            catch
            {
                throw;
            }
			
        }

        public IQueryable<Product> GetAll()
        {
            try
            {
                return Database.Products.GetAll();

            }
            catch
            {
                throw;
            }
			
        }

		public void CreateProduct(Product product)
		{
            try
            {
                Database.Products.Create(product);
                Database.Save();
            }
            catch
            {
                throw;
            }

        }

		public void Update(Product product)
		{
            
            try
            {
                Database.Products.Update(product);
                Database.Save();
            }
            catch
            {
                throw;
            }
			
		}

        public void Delete(int id)
        {
            try
            {
                Database.Products.Delete(id);
                Database.Save();
            }
            catch
            {
                throw;
            }
        }

		public void Dispose()
		{
			Database.Dispose();
		}

    
    }
}
