﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using VetrovShop.Models;

namespace VetrovShop.Services.Interfaces
{
    public interface IProductService
    {
        void CreateProduct(Product product);
		void Update(Product product);
        Product FindProduct(int id);
        IQueryable<Product> GetAll();
        void Delete(int id);
        void Dispose();
    }
}
