﻿using System;
using System.Collections.Generic;
using System.Text;
using VetrovShop.Models;
using System.Linq;

namespace VetrovShop.Services.Interfaces
{
    public interface IOrderService
    {
        void CreateOrder(Order order);
        List<OrderInfo> OrderInfo(string UserId);
        void Dispose();
    }
}
