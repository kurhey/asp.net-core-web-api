﻿using System;
using System.Collections.Generic;
using System.Text;
using VetrovShop.Models;

namespace VetrovShop.Services.Interfaces
{
    public interface IProductOrderService
    {
        void MakeOrder(ProductOrder order);
        ProductOrder GetProduct(int? id);
        IEnumerable<ProductOrder> GetProducts();
        void Dispose();
    }
}
