﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VetrovShop.Services.Validation
{
    class ValidationExeption : Exception
    {
        public string Property { get; protected set; }

        public ValidationExeption(string msg, string prop) : base(msg)
        {
            Property = prop;
        }
    }
}
