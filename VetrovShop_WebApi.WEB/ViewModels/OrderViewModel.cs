﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using VetrovShop.Models;

namespace VetrovShop.WEB.ViewModels
{
    public class OrderViewModel
    {
        public int OrderId { get; set; }
        public List<Product> Products { get; set; }
    }
}
