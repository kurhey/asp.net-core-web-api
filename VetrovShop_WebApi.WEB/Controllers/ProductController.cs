﻿using VetrovShop.Models;
using System.Net.Http;
using VetrovShop.Services.Interfaces;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using VetrovShop.DAL.Identity;

namespace VetrovShop.WEB.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ProductController : Controller
    {
        private static readonly HttpClient client = new HttpClient();
        private IProductService productService;


        public ProductController(IProductService service)
        {
            productService = service;
        }

        [HttpGet]
        public IActionResult Index()
        {
            try
            {
                var products = productService.GetAll();
                return Ok(products);
            }
            catch(Exception ex)
            {
                return StatusCode(500);
            }
        }

        [HttpPost]
        //[Authorize(Roles = "Admin")]
        public IActionResult Create(Product product)
        {
            if (product == null) return BadRequest();
            product.DateUpd = DateTime.Now;
            try
            {
                productService.CreateProduct(product);
                return Ok();
            }
            catch (Exception ex)
            {
                return StatusCode(500);
            }
        }

        //[HttpGet]
        //[Authorize(Roles = "Admin")]
        //public IActionResult Update(int id) => View("UpdateProduct", productService.FindProduct(id));


        //[HttpPost]
        //[Authorize(Roles = "Admin")]
        //public IActionResult Update(Product product)
        //{
        //    try
        //    {
        //        productService.Update(product);
        //        return RedirectToAction("Index");
        //    }
        //    catch(Exception ex)
        //    {
        //        ViewBag.Error = ex.Message;
        //        return View("Error");
        //    }

        //}

        //[HttpPost]
        //[Authorize(Roles = "Admin")]
        //public IActionResult Delete(int id)
        //{
        //    try
        //    {
        //        productService.Delete(id);
        //        return RedirectToAction("Index");
        //    }
        //    catch (Exception ex)
        //    {
        //        return View("Error");
        //    }

        //}
    }
}