﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using VetrovShop.Models;

namespace VetrovShop.WEB.Controllers
{
    public class UserController : Controller
    {
        private UserManager<User> _UserManager;

        public UserController(UserManager<User> _UserManager)
        {
            this._UserManager = _UserManager;
        }

        public async Task<IActionResult> Index()
        {
            var user = await _UserManager.GetUserAsync(User);
            return View(user);
        }
    }
}