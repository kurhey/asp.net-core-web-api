﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using VetrovShop.DAL.Interfaces;
using VetrovShop.DAL.EF;
using VetrovShop.Models;

namespace VetrovShop.DAL.Repositories
{
    public class ProductOrderRepository : IRepository<ProductOrder>
    {

        private ApplicationContext db;

        public ProductOrderRepository(ApplicationContext context)
        {
            this.db = context;
        }

        public void Create(ProductOrder item)
        {
            db.ProductOrders.Add(item);
        }

        public void Delete(int id)
        {
            ProductOrder productOrder = db.ProductOrders.Find(id);
            if (productOrder != null)
                db.ProductOrders.Remove(productOrder);
        }

        public IEnumerable<ProductOrder> FindByCondition(Func<ProductOrder, bool> predicate)
        {
            return db.ProductOrders.Where(predicate).ToList();
        }

        public ProductOrder FindById(int id)
        {
            return db.ProductOrders.Find(id);
        }

        public IQueryable<ProductOrder> GetAll()
        {
            return db.ProductOrders;
        }

        public void Update(ProductOrder item)
        {
            db.Entry(item).State = EntityState.Modified;
        }
    }
}
