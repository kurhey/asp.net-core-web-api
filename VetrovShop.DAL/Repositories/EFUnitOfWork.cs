﻿
using Microsoft.EntityFrameworkCore;
using System;
using VetrovShop.DAL.EF;
using VetrovShop.Models;
using VetrovShop.DAL.Interfaces;
using Microsoft.AspNetCore.Identity;

namespace VetrovShop.DAL.Repositories
{
    public class EFUnitOfWork : IUnitOfWork
    {
        private ApplicationContext db;

        private OrderRepository OrderRepository;
        private ProductOrderRepository ProductOrderRepository;
        private ProductRepository ProductRepository;

        public EFUnitOfWork(ApplicationContext options)
        {
            db = options;
        }

        public EFUnitOfWork()
        {

        }

        public IRepository<Order> Orders
        {
            get
            {
                if (OrderRepository == null)
                    OrderRepository = new OrderRepository(db);
                return OrderRepository;
            }
        }

        public IRepository<Product> Products
        {
            get
            {
                if (ProductRepository == null)
                    ProductRepository = new ProductRepository(db);
                return ProductRepository;
            }
        }

        public IRepository<ProductOrder> ProductOrders
        {
            get
            {
                if (ProductOrderRepository == null)
                    ProductOrderRepository = new ProductOrderRepository(db);
                return ProductOrderRepository;
            }
        }

        public void Dispose()
        {
			db.Dispose();
        }

        public void Save()
        {
            try
            {
                db.SaveChanges();
            }
            catch
            {
                throw;
            }
			
        }
    }
}
