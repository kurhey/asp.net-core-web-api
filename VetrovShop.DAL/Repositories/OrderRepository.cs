﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using VetrovShop.DAL.EF;
using VetrovShop.DAL.Interfaces;
using VetrovShop.Models;

namespace VetrovShop.DAL.Repositories
{
    public class OrderRepository : IRepository<Order>
    {

        private readonly ApplicationContext db;

        public OrderRepository(ApplicationContext context)
        {
            this.db = context;
        }

        public void Create(Order item)
        {
            db.Orders.Add(item);
        }

        public void Delete(int id)
        {
            Order order = db.Orders.Find(id);

            if(order != null)
                db.Orders.Remove(order);
        }

        public IEnumerable<Order> FindByCondition(Func<Order, bool> predicate)
        {
            return db.Orders.Where(predicate).ToList();
        }

        public Order FindById(int id)
        {
            try
            {
                return db.Orders.Find(id);
            }
            catch
            {
                throw;
            }
            
        }

        public IQueryable<Order> GetAll()
        {
            try
            {
                return db.Orders;
            }
            catch
            {
                throw;
            }
            
        }

        public void Update(Order item)
        {
            db.Entry(item).State = EntityState.Modified;
        }
    }
}
