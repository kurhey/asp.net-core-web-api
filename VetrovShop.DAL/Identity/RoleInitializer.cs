﻿using Microsoft.AspNetCore.Identity;
using System.Threading.Tasks;
using VetrovShop.DAL.EF;
using VetrovShop.Models;

namespace VetrovShop.DAL.Identity
{
    public class RoleInitializer 
    {
        static string superrole = "Superuser";
        static string defaultrole = "Customer";
        static string adminrole = "Admin";

        public static string GetSuperRole
        {
            get
            {
                return superrole;
            }
        }

        public static string GetDefaultRole
        {
            get
            {
                return defaultrole;
            }
        }

        public static string GetAdminRole
        {
            get
            {
                return adminrole;
            }
        }

        public static async Task InitializeRoleAsync(UserManager<User> _UserManager, RoleManager<IdentityRole> _RoleManager)
        {
            if (await _RoleManager.FindByNameAsync(superrole) == null)
                await _RoleManager.CreateAsync(new IdentityRole(superrole));



            if (await _RoleManager.FindByNameAsync(defaultrole) == null)
                await _RoleManager.CreateAsync(new IdentityRole(defaultrole));

            if (await _RoleManager.FindByNameAsync(adminrole) == null)
                await _RoleManager.CreateAsync(new IdentityRole(adminrole));

            await CreateSuperUserAsync(_UserManager, superrole);
        }

        public static async Task CreateSuperUserAsync(UserManager<User> _UserManager, string role)
        {
            string adminEmail = "superuser@gmail.com";
            string password = "superuser";

            if( await _UserManager.FindByEmailAsync(adminEmail) == null)
            {
                User superuser = new User { Email = adminEmail, UserName = adminEmail, EmailConfirmed = true };
                var result = await _UserManager.CreateAsync(superuser, password);

                if (result.Succeeded) await _UserManager.AddToRoleAsync(superuser, role);
            }
        }
    }
}
