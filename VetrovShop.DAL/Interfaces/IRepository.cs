﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;

namespace VetrovShop.DAL.Interfaces
{
    public interface IRepository<T> where T: class
    {
        IQueryable<T> GetAll();
        T FindById(int id);
        IEnumerable<T> FindByCondition(Func<T, Boolean> predicate);
        void Create(T item);
        void Update(T item);
        void Delete(int id);
	}
}
